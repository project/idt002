IDThemes ChangeLog

== IDThemes idt002 ==

===2009-07-10 : 1.1 ===

Improved:
- The file/images/Header.jpg is removed. This file is not used now.
- The file/images/logo.png is added.
- The file screenshot.png is updated.
- Changes in style.css which concern Header.jpg are made.
- The file releasenotes.txt is updated. The Themes are already tested on Mozilla Firefox 3.5

===2009-06-27 : 1.0 ===

The first public release of a idt002 Drupal themes collection