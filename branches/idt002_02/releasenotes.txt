
IDThemes theme release notes
==================================

Please see the full changelog for details in changelog.txt.


The Theme is compatible with CMS
==================================

Drupal
		Releases: Drupal 5, Drupal 6
		
Also themes are compatible to others young developing CMS, based on Drupal


The Themes are already tested on browsers:
------------------------
- Mozilla Firefox 3.5
- Opera 9.64
- Google Chrome 1.0.154.48
- Safari 4 (for Windows)
- Mozilla SeaMonkey 1.1.14
- Maxthon 2.5.1.4751
- Internet Explorer 6,7,8

The Theme is already tested at screen resolution:
------------------------
- 1024x768 px
- 1280x800 px
- 1280x1024 px


Downloading theme by IDThemes
==================================

All releases can be downloaded here: 
http://sourceforge.net/project/showfiles.php?group_id=264184


Checking out IDThemes files from the Git
==================================

The path for all releases is the following:
http://idthemes.git.sourceforge.net/git/gitweb.cgi?p=idthemes;a=tree


Checking out IDThemes files from the SVN
==================================

You can easily get IDThemes files using SVN. Instructions about this can be found here:
http://sourceforge.net/svn/?group_id=264184

The path for this release is the following:
https://idthemes.svn.sourceforge.net/svnroot/idthemes/themes/idt<collection id>/tags/idt<collection id>_<theme name>_<release number>/idt<collection id>_<theme subname>/

If you feel like getting the latest "edge" version, you can checkout the theme branch at :
https://idthemes.svn.sourceforge.net/svnroot/idthemes/themes/idt<collection id>/branches/idt<collection id>_<theme name>/idt<collection id>_<theme subname>/

The IDThemes branch is always the latest as-stable-as-possible development code. It is, of course, not to be used on production sites.


Installing theme by IDThemes
==================================

1. Access your Web server using an FTP client or Web server administration tools.
2. Create a folder for your specific theme under "<YourSiteFolder>/themes/" folder within Drupal installation.
   For example: <YourSiteFolder>/themes/<MyNewTheme>
3. Copy or upload theme files exported from repository into the newly created <MyNewTheme> folder.
4. Login to your Drupal Administer.
5. Go to Drupal Administer -> Site Building -> Themes (www.YourSite.com/?q=admin/build/themes)
6. Select your newly uploaded theme from the list of available themes for your site.
7. Click the "Save configuration" button to save your changes.
For more information please visit: http://drupal.org/node/456

Utilizing Menus
==================================

Please use the following steps to utilize menu style: 
1. Go to Drupal Administer -> Site Building -> Menus (www.YourSite.com/?q=admin/build/menu) 
2. Edit an existing menu or create a new one. 
3. Go to Drupal Administer -> Site Building -> Blocks (www.YourSite.com/?q=admin/build/block)
4. Place menu into the "Menu" region.

Customizing the Footer
==================================
To customize the theme footer via Drupal Administer place one or multiple blocks into the "Copyright" region.
Here are sample steps to configure custom footer:
1. Go to Drupal Administer -> Site configuration -> Site information 
   (www.your-site.com/?q=admin/settings/site-information)
2. Edit the Footer message field.
3. Save your changes.


Where to find more information ?
==================================

The IDThemes Network
------------------------
Sourceforge Project: http://www.sourceforge.net/projects/idthemes
Home of IDThemes: http://idthemes.sourceforge.net

IDThemes Mailing Lists
------------------------

A few mailing lists are available for everyone to stay informed regarding development information, bug tracking, feature requests, etc...
Simply go here to subscribe to the desired mailing lists: http://sourceforge.net/mail/?group_id=264184

IDThemes Related Sites
------------------------
Ohloh project: http://www.ohloh.net/p/idthemes/
Freshmeat project: http://freshmeat.net/projects/idthemes/

==================================
The IDThemes Project
http://idthemes.sourceforge.net
