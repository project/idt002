<div class="idt-Post">
    <div class="idt-Post-body">
<div class="idt-Post-inner">
<h2 class="idt-PostHeaderIcon-wrapper"> <span class="idt-PostHeader"><a href="<?php echo $node_url; ?>" title="<?php echo $title; ?>"><?php echo $title; ?></a></span>
</h2>
<?php if ($submitted): ?>
<div class="idt-PostHeaderIcons idt-metadata-icons">
<?php echo idt_submitted_worker($submitted, $date, $name); ?>

</div>
<?php endif; ?>
<div class="idt-PostContent">
<div class="idt-article"><?php echo $content;?>
   <?php if (isset($node->links['node_read_more'])) { echo idtReadMoreLink($node->links['node_read_more']); }?></div>
</div>
<div class="cleared"></div>
<?php if (isidtLinksSet($links) || !empty($terms)): ?>
<div class="idt-PostFooterIcons idt-metadata-icons">
<?php if (!empty($links)) { echo idt_links_woker($node->links);} ?>
<?php if (!empty($terms)) { echo idt_terms_worker($node);} ?>

</div>
<?php endif; ?>

</div>

    </div>
</div>
